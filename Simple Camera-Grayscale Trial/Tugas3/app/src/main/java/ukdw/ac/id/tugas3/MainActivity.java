package ukdw.ac.id.tugas3;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.ColorMatrix;
import android.graphics.ColorMatrixColorFilter;
import android.graphics.Paint;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.text.Editable;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;


public class MainActivity extends ActionBarActivity {

    private static final int foto_code = 100;
    public static final int media_foto = 1;

    Button btnCamera, btnGrayScale, btnNewFolder, btnSave;
    ImageView imageView;
    String path;
    Uri selectedImage, fileUri;
    File file;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btnCamera = (Button)findViewById(R.id.btnCamera);
        btnGrayScale = (Button)findViewById(R.id.btnGrayscale);
        btnNewFolder = (Button)findViewById(R.id.btnNewFolder);
        btnSave = (Button)findViewById(R.id.btnSave);

        imageView = (ImageView)findViewById(R.id.imageView);

        btnCamera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                fileUri = getOutputMediaFileUri(media_foto);
                intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);
                startActivityForResult(intent,foto_code);
            }
        });

        btnGrayScale.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bitmap bitmap = ((BitmapDrawable)imageView.getDrawable()).getBitmap();
                imageView.setImageBitmap(toGrayscale(bitmap));
            }
        });

        btnNewFolder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder alert = new AlertDialog.Builder(MainActivity.this);

                alert.setTitle("Title");
                alert.setMessage("Message");

                final EditText input = new EditText(MainActivity.this);
                alert.setView(input);

                alert.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Editable value = input.getText();

                        file = new File(Environment.getExternalStorageDirectory()+"/"+value);
                        file.mkdirs();
                        Toast.makeText(getApplicationContext(),"folder berhasil dibuat", Toast.LENGTH_LONG).show();
                    }
                });
                alert.show();
            }
        });

        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //imageView.setDrawingCacheEnabled(true);
                imageView.buildDrawingCache();
                final Bitmap bitmap = imageView.getDrawingCache();

                OutputStream fOut = null;
                Uri outputFileUri;
                try{
                    File root = new File(Environment.getExternalStorageDirectory()+File.separator+"folder_name"+File.separator);
                    root.mkdirs();
                    File sdImageMainDeirectory = new File(root, "myPicName.jpg");
                    outputFileUri = Uri.fromFile(sdImageMainDeirectory);
                    fOut = new FileOutputStream(sdImageMainDeirectory);
                    Toast.makeText(getApplicationContext(),"Saved",Toast.LENGTH_SHORT).show();
                } catch (FileNotFoundException e) {
                    //e.printStackTrace();
                    Toast.makeText(getApplicationContext(), "Error occured. Please try again later.",Toast.LENGTH_SHORT).show();
                }
                try {
                    bitmap.compress(Bitmap.CompressFormat.PNG, 100, fOut);
                    fOut.flush();
                    fOut.close();
                } catch (Exception e) {
                }
            }
        });
    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent imageReturnedIntent){
//        super.onActivityResult(requestCode, resultCode, imageReturnedIntent);
//
//        if (resultCode==RESULT_OK){
//            selectedImage = imageReturnedIntent.getData();
//            imageView.setImageURI(selectedImage);
////            Toast.makeText(getApplicationContext(), selectedImage.toString(), Toast.LENGTH_LONG).show();
////            Toast.makeText(getBaseContext(), getImagePath(selectedImage), Toast.LENGTH_LONG).show();
//            path = getImagePath(selectedImage);
//
//        }
        if (requestCode == foto_code){
            if (resultCode == RESULT_OK){
                System.out.println("Masuk sini lho");
                previewCapturedImage();
            }
            else if (resultCode == RESULT_CANCELED){
                Toast.makeText(getApplicationContext(),""+"gagal mengambil foto", Toast.LENGTH_LONG).show();
            }
            else{
                Toast.makeText(getApplicationContext(),""+"gagal mengambil foto", Toast.LENGTH_LONG).show();
            }
        }
    }

    private String getImagePath(Uri uri) {
        Cursor cursor = getContentResolver().query(uri, null, null, null, null);
        cursor.moveToFirst();
        String document_id = cursor.getString(0);
        document_id = document_id.substring(document_id.lastIndexOf(":")+1);
        cursor.close();

        cursor = getContentResolver().query(MediaStore.Images.Media.EXTERNAL_CONTENT_URI,null,MediaStore.Images.Media._ID+" = ? ", new String[]{document_id},null);
        cursor.moveToFirst();
        String path = cursor.getString(cursor.getColumnIndex(MediaStore.Images.Media.DATA));
        cursor.close();

        return path;
    }

    public Bitmap toGrayscale(Bitmap bmpOriginal){
        int width, height;
        height = bmpOriginal.getHeight();
        width = bmpOriginal.getWidth();

        Bitmap bmpGrayscale = Bitmap.createBitmap(width, height, Bitmap.Config.RGB_565);
        Canvas c = new Canvas(bmpGrayscale);
        Paint paint = new Paint();
        ColorMatrix cm = new ColorMatrix();
        cm.setSaturation(0);
        ColorMatrixColorFilter f = new ColorMatrixColorFilter(cm);
        paint.setColorFilter(f);
        c.drawBitmap(bmpOriginal,0,0,paint);
        return bmpGrayscale;
    }

//    kamera
    public Uri getOutputMediaFileUri(int type){
        System.out.println(Uri.fromFile(getOutputMediaFile(type)));
        return Uri.fromFile(getOutputMediaFile(type));
    }

    private File getOutputMediaFile(int type){
        File mediaStorageDir = new File(Environment.getExternalStorageDirectory()+"/ilate");
        mediaStorageDir.mkdirs();
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.getDefault()).format(new Date());
        File mediaFile;
        if (type==media_foto){
            mediaFile = new File(mediaStorageDir.getPath()+File.separator+"IMG_"+timeStamp+".jpg");
            System.out.print(mediaStorageDir.getPath()+File.separator+"IMG_"+timeStamp+".jpg");
            MainActivity.this.sendBroadcast(new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE, Uri.parse("file://"+mediaStorageDir)));
        }
        else {
            return null;
        }
        return mediaFile;
    }

    private void previewCapturedImage(){
        try{
            BitmapFactory.Options opt = new BitmapFactory.Options();
            opt.inSampleSize = 0;
            final Bitmap bitmap = BitmapFactory.decodeFile(fileUri.getPath(),opt);
            imageView.setImageBitmap(bitmap);
        }
        catch (NullPointerException e){
            e.printStackTrace();
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState){
        super.onSaveInstanceState(outState);
        outState.putParcelable("file_uri",fileUri);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState){
        super.onRestoreInstanceState(savedInstanceState);
        fileUri = savedInstanceState.getParcelable("file_uri");
    }

//    kamera
}
