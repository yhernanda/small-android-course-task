package id.ac.ukdw.cobamap;

import android.app.ListActivity;
import android.content.Intent;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationManager;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;
import java.util.List;


public class MainActivity extends ListActivity {
    MarkerOptions marker;
    List Marks;
    Double latitudeC = 0.0;
    Double longitudeC = 0.0;
    Location[] loc;
    List MarksTitle;
    List MarksDesc;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        MarksTitle = new ArrayList();
        MarksTitle.add("Angkringan Lik Man");
        MarksTitle.add("Oseng-Oseng Mercon Bu Narti");
        MarksTitle.add("Soto Betawi Bang H. Pitung");
        MarksTitle.add("Gudeg Permata");
        MarksTitle.add("Bale Ayu Resto");

        MarksDesc = new ArrayList();
        MarksDesc.add("Lik Man itu Pria");
        MarksDesc.add("Osengnya meledak-ledak");
        MarksDesc.add("Masaknya sambil Silat");
        MarksDesc.add("Keselek Permata");
        MarksDesc.add("Kenangan bersama Bu Her");

        LocationManager locationManager = (LocationManager)getSystemService(LOCATION_SERVICE);
        String provider = locationManager.getBestProvider(new Criteria(), true);
        loc = new Location[5];
        loc[0] = new Location(provider);
        loc[0].setLatitude(-7.78894444444444);
        loc[0].setLongitude(110.365888888889);
        loc[1] = new Location(provider);
        loc[1].setLatitude(-7.80111111111111);
        loc[1].setLongitude(110.357777777778);
        loc[2] = new Location(provider);
        loc[2].setLatitude(-7.79686666666667);
        loc[2].setLongitude(110.37165);
        loc[3] = new Location(provider);
        loc[3].setLatitude(-7.80125);
        loc[3].setLongitude(110.373108333333);
        loc[4] = new Location(provider);
        loc[4].setLatitude(-7.79561111111111);
        loc[4].setLongitude(110.393055555556);

        ArrayAdapter adapter = new ArrayAdapter(this,
            android.R.layout.simple_list_item_2, android.R.id.text1, MarksTitle);
        setListAdapter(adapter);
}
    @Override
    protected void onListItemClick(ListView l, View v, int position, long id) {
        Intent intent = new Intent(this, MapActivity.class);
        intent.putExtra("latitude", loc[position].getLatitude());
        intent.putExtra("longitude", loc[position].getLongitude());
        intent.putExtra("title", MarksTitle.get(position).toString());
        intent.putExtra("desc", MarksDesc.get(position).toString());
        startActivity(intent);
    }
}
