package id.ac.ukdw.cobamap;

import android.annotation.TargetApi;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationManager;
import android.media.audiofx.BassBoost;
import android.os.Build;
import android.provider.Settings;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.google.android.gms.location.LocationListener;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;


public class MapActivity extends Activity implements LocationListener{

    // Google Map
    private GoogleMap googleMap;
    LocationManager lm;
    Location location;
    Double latitudeC = 0.0;
    Double longitudeC = 0.0;
    MarkerOptions marker2;
    LatLng latlng;
    String title;
    String desc;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map);

        setUpMap();
        lm = (LocationManager)getSystemService(LOCATION_SERVICE);
        String provider = lm.getBestProvider(new Criteria(), true);
        Double latitude = (Double) this.getIntent().getDoubleExtra("latitude", 0.0);
        Double longitude = (Double) this.getIntent().getDoubleExtra("longitude", 0.0);
        location = new Location(provider);
        location.setLatitude(latitude);
        location.setLongitude(longitude);
        title = this.getIntent().getCharSequenceExtra("title").toString();
        desc = this.getIntent().getCharSequenceExtra("desc").toString();
        onLocationChanged(location);
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    private void setUpMap()
    {
        // Do a null check to confirm that we have not already instantiated the map.
        if (googleMap == null)
        {
            // Try to obtain the map from the SupportMapFragment.
            googleMap = ((MapFragment) getFragmentManager().findFragmentById(R.id.map)).getMap();

            // Check if we were successful in obtaining the map.

            if (googleMap != null)
            {
                googleMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener()
                {
                    @Override
                    public boolean onMarkerClick(com.google.android.gms.maps.model.Marker marker)
                    {
                        marker.showInfoWindow();
                        return true;
                    }
                });
            }
            else
                Toast.makeText(getApplicationContext(), "Unable to create Maps", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onLocationChanged(Location location){
        latitudeC = location.getLatitude();
        longitudeC = location.getLongitude();
        latlng = new LatLng(latitudeC, longitudeC);
        if(marker2 == null){
            marker2 = new MarkerOptions().position(new LatLng(latitudeC, longitudeC)).title(title).snippet(desc);
            googleMap.addMarker(marker2);
            CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(latlng, 10);
            googleMap.animateCamera(cameraUpdate);
        } else{
            CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(latlng, 10);
            googleMap.animateCamera(cameraUpdate);
        }
    }
}
