package id.ac.ukdw.tugas6;

import android.app.Activity;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.graphics.drawable.ShapeDrawable;
import android.graphics.drawable.shapes.OvalShape;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.view.View;
import java.util.Random;

public class MainActivity extends Activity implements SensorEventListener {
    public static int x=0;
    public static int y=0;
    public static int xRandom = 100 ;
    public static int yRandom = 200 ;

    //flag
    public static boolean nabrak = false;
    public static int score = 0;

    CustomDrawableView mCustomDrawableView = null;
    ShapeDrawable mDrawable = new ShapeDrawable();
    private SensorManager sensorManager = null;

    @Override
    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        sensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);
        mCustomDrawableView = new CustomDrawableView(this);
        setContentView(mCustomDrawableView);
    }

    public void onSensorChanged(SensorEvent sensorEvent){
        if (sensorEvent.sensor.getType() == Sensor.TYPE_ACCELEROMETER){
            x -= sensorEvent.values[0];
            y += sensorEvent.values[1];

            if(x < xRandom+80 && x > xRandom-80 && y < yRandom+80  && y > yRandom-80){
                score += 10;
                nabrak = false;
            }
        }

        if (sensorEvent.sensor.getType() == Sensor.TYPE_ORIENTATION){

        }
     }

    public void onAccuracyChanged(Sensor arg0, int arg1){
        // TODO Auto-generated method stub
    }

    @Override
    protected void onResume(){
        super.onResume();
        sensorManager.registerListener(this, sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER),
                SensorManager.SENSOR_DELAY_GAME);
    }

    @Override
    protected void onStop(){
        sensorManager.unregisterListener(this);
        super.onStop();
    }

    public class CustomDrawableView extends View{
        static final int width = 100;
        static final int height = 100;

        public CustomDrawableView(Context context){
            super(context);
            mDrawable = new ShapeDrawable(new OvalShape());
            mDrawable.getPaint().setColor(0xff74AC23);
            mDrawable.setBounds(x, y, x + width, y + height);
        }

        protected void onDraw(Canvas canvas){
            Paint shadowPaint = new Paint();
            shadowPaint.setStyle(Paint.Style.STROKE);
            shadowPaint.setTextSize(70.0f);
            shadowPaint.setColor(Color.BLUE);
            shadowPaint.setShadowLayer(5.0f, 5.0f, 5.0f, Color.GRAY);
            canvas.drawText("Score : "+ score, 72, 140, shadowPaint);

            if(!nabrak){
                nabrak = true;
                Random rand = new Random();
                xRandom = rand.nextInt(canvas.getWidth()-100);
                yRandom = rand.nextInt(canvas.getHeight()-100);
            }

            RectF ball = new RectF(MainActivity.x, MainActivity.y, MainActivity.x + width, MainActivity.y + height);
            RectF ballRandom = new RectF(xRandom, yRandom, xRandom + width, yRandom + height);

            Paint p = new Paint();
            Paint b = new Paint();

            p.setColor(Color.rgb(224, 31, 156));
            canvas.drawOval(ball, p);

            p.setColor(Color.rgb(231, 24, 143));
            canvas.drawOval(ballRandom, b);

            invalidate();
        }
    }
}